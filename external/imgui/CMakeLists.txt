# SPDX-FileCopyrightText: 2021 Roman Putanowicz <putanowr@gmail.com>
# SPDX-License-Identifier: MIT

cmake_minimum_required(VERSION 3.11)

set(IMGUI_PUBLIC_HEADERS 
imconfig.h
imgui.h
imgui_impl_glfw.h
imgui_impl_opengl3.h
)

set(IMGUI_SOURCES 
imgui.cpp
imgui_demo.cpp
imgui_draw.cpp
imgui_tables.cpp
imgui_widgets.cpp
imgui_internal.h
imstb_rectpack.h
imstb_textedit.h
imstb_truetype.h
imgui_impl_glfw.cpp
imgui_impl_opengl3.cpp
)

add_library(imgui STATIC ${IMGUI_SOURCES} ${IMGUI_PUBLIC_HEADERS})

target_include_directories(imgui PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${GLUT_INCLUDE_DIRS}
  ${OPENGL_INCLUDE_DIR}
)

target_compile_definitions(imgui PUBLIC UNICODE _UNICODE)

set_target_properties(imgui PROPERTIES FOLDER libraries)
set_target_properties(imgui PROPERTIES PUBLIC_HEADER "${IMGUI_PUBLIC_HEADERS}")

target_link_libraries(imgui PUBLIC glfw gl3w ${OPENGL_LIBRARIES})
