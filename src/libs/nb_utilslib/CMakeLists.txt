# SPDX-FileCopyrightText: 2021 Roman Putanowicz <putanowr@gmail.com>
# SPDX-License-Identifier: MIT

cmake_minimum_required(VERSION 3.11)

set(NB_UTILSLIB_PUBLIC_HEADERS 
  easylogging++.h
  nb_LoggingUtils.h
)

set(NB_UTILSLIB_SOURCES 
  easylogging++.cc
  nb_LoggingUtils.cpp
)

add_library(nb_utilslib STATIC ${NB_UTILSLIB_SOURCES} ${NB_UTILSLIB_PUBLIC_HEADERS})

target_include_directories(nb_utilslib PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}
)

target_compile_definitions(nb_utilslib PUBLIC ELPP_FRESH_LOG_FILE)
target_compile_definitions(nb_utilslib PUBLIC UNICODE _UNICODE)

set_target_properties(nb_utilslib PROPERTIES FOLDER libraries)
set_target_properties(nb_utilslib PROPERTIES PUBLIC_HEADER "${NB_UTILSLIB_PUBLIC_HEADERS}")
