// SPDX-FileCopyrightText: 2021 Roman Putanowicz <putanowr@gmail.com>
// SPDX-License-Identifier: MIT

#pragma once

#include "nb_DebugRenderer.h"

#define DEBUG_DRAW_IMPLEMENTATION

namespace nb_ddraw
{

void DebugRenderer::setupView()
{
}

void DebugRenderer::render()
{
}

void DebugRenderer::drawBox()
{
}

void initialize(DebugRenderer* pRenderer) {
}

void shutdown() {
}

DebugRenderer::DebugRenderer()
{
}

DebugRenderer::~DebugRenderer()
{
}

} // namespace nb_ddraw
