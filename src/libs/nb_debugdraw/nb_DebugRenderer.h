// SPDX-FileCopyrightText: 2021 Roman Putanowicz <putanowr@gmail.com>
// SPDX-License-Identifier: MIT

//!
//! @file
//! @brief Provide utilities for quick and dirty drawing for debugging purposes.
//!

#pragma once
#include <string>
#include "vectormath.hpp"

namespace nb_ddraw
{

class DebugRenderer
{
	public:
	DebugRenderer();
	~DebugRenderer();
	void setupView();
	void render();
	void drawBox();
};

void initialize(DebugRenderer* pRenderer);
void shutdown();

} // namespace nb_utils
