// SPDX-FileCopyrightText: 2021 Roman Putanowicz <putanowr@gmail.com>
// SPDX-License-Identifier: MIT

#include "easylogging++.h"
#include "nb_application.h"

INITIALIZE_EASYLOGGINGPP

#include <stdlib.h>

int main(int argc, char *argv[])
{
	LOG(INFO) << "Running NewtonBodies";
	if (!nb_app::Application::initialize())
	{
		LOG(ERROR) << "Application initialization failed";
		return EXIT_FAILURE;
	};
	nb_app::Application app;
	app.run();
	return EXIT_SUCCESS;
}
