// SPDX-FileCopyrightText: 2021 Roman Putanowicz <putanowr@gmail.com>
// SPDX-License-Identifier: MIT

#pragma once

struct GLFWwindow;

namespace nb_app
{

//!
//! @brief
//!
//!
class Application
{
public:
	static bool initialize();

	Application();
	/**
	 * @brief Run the application.
	 *
	 */
	~Application();
	void run();
private:
	bool _buildGUI();
	void _beginFrame();
	void _endFrame();
	void _renderContent();
	void _renderWorld();
	void _renderGUI();

	GLFWwindow* _mainFrame = nullptr;

	static const char* _glsl_version;
  };
}
